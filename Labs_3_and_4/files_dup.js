var files_dup =
[
    [ "closed_loop_controller.py", "closed__loop__controller_8py.html", [
      [ "closed_loop_controller.ClosedLoop", "classclosed__loop__controller_1_1ClosedLoop.html", "classclosed__loop__controller_1_1ClosedLoop" ]
    ] ],
    [ "DRV8847.py", "DRV8847_8py.html", "DRV8847_8py" ],
    [ "encoder.py", "encoder_8py.html", [
      [ "encoder.Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "main.py", "main_8py.html", null ],
    [ "shares.py", "shares_8py.html", [
      [ "shares.Share", "classshares_1_1Share.html", "classshares_1_1Share" ],
      [ "shares.Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ]
    ] ],
    [ "task_closed_loop_controller.py", "task__closed__loop__controller_8py.html", "task__closed__loop__controller_8py" ],
    [ "task_encoder.py", "task__encoder_8py.html", "task__encoder_8py" ],
    [ "task_motor.py", "task__motor_8py.html", "task__motor_8py" ],
    [ "task_motorDriver.py", "task__motorDriver_8py.html", "task__motorDriver_8py" ],
    [ "task_user.py", "task__user_8py.html", "task__user_8py" ]
];