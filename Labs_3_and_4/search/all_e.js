var searchData=
[
  ['s0_5finit_0',['S0_init',['../task__encoder_8py.html#a118da909b013744f2b2ef4febcca4e32',1,'task_encoder.S0_init()'],['../task__motor_8py.html#aaa32437a8951515fe5ce41df8fe1e298',1,'task_motor.S0_init()'],['../task__user_8py.html#a09d12fac9d0696e1f015737e65327368',1,'task_user.S0_init()']]],
  ['ser_1',['ser',['../classtask__encoder_1_1Task__Encoder.html#a84b0e80344084871098b90dfd111fc93',1,'task_encoder.Task_Encoder.ser()'],['../classtask__user_1_1Task__User.html#ae1e7bc8b7b912e51605e649affd85438',1,'task_user.Task_User.ser()']]],
  ['set_5fencoder_5fid_2',['set_encoder_ID',['../classencoder_1_1Encoder.html#a163f72a80c7634d857f45f02c5889a20',1,'encoder::Encoder']]],
  ['set_5fkp_3',['set_Kp',['../classclosed__loop__controller_1_1ClosedLoop.html#aed78d6e002f731c0a6d2f95c33020865',1,'closed_loop_controller::ClosedLoop']]],
  ['set_5fposition_4',['set_position',['../classencoder_1_1Encoder.html#a097746ac59abf28e6567f5604fe83c1f',1,'encoder::Encoder']]],
  ['setduty_5',['setDuty',['../classDRV8847_1_1Motor.html#a3a4462b2716495a75b3dca4b02417ec4',1,'DRV8847::Motor']]],
  ['share_6',['Share',['../classshares_1_1Share.html',1,'shares']]],
  ['shares_2epy_7',['shares.py',['../shares_8py.html',1,'']]],
  ['state_8',['state',['../classtask__encoder_1_1Task__Encoder.html#af038aa706137ba698bf272c011091e50',1,'task_encoder.Task_Encoder.state()'],['../classtask__user_1_1Task__User.html#afefb79be360ac39f0ed9920de91f953e',1,'task_user.Task_User.state()']]]
];
