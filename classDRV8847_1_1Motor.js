var classDRV8847_1_1Motor =
[
    [ "__init__", "classDRV8847_1_1Motor.html#af2865a63b46543f4250961c76f07eb3a", null ],
    [ "brake", "classDRV8847_1_1Motor.html#afc1bd4b8da320cccb14dd3b7f2c4f535", null ],
    [ "coast", "classDRV8847_1_1Motor.html#a92b771fb75b06ed6be216cecf8808130", null ],
    [ "getDirection", "classDRV8847_1_1Motor.html#a811a89ce4d35fc85cc0924065db50906", null ],
    [ "getDuty", "classDRV8847_1_1Motor.html#a812fa4f216455c4bf6583063850ef923", null ],
    [ "getMotorID", "classDRV8847_1_1Motor.html#a62af574d21e51c673e7e47c769d2a099", null ],
    [ "getRunState", "classDRV8847_1_1Motor.html#adbee4c8b1a03926553477a6eea0a8c5b", null ],
    [ "setDuty", "classDRV8847_1_1Motor.html#a3a4462b2716495a75b3dca4b02417ec4", null ],
    [ "toggleRunState", "classDRV8847_1_1Motor.html#a4fb478f1d1550959b44fe3168d8378a5", null ]
];