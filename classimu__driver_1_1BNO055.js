var classimu__driver_1_1BNO055 =
[
    [ "__init__", "classimu__driver_1_1BNO055.html#adec6e7a7bf73fb424ed94cc5c583305b", null ],
    [ "get_calibrationCoef", "classimu__driver_1_1BNO055.html#a12b458256a512e9b57ef3d351152b2b1", null ],
    [ "get_CalibrationStatus", "classimu__driver_1_1BNO055.html#a39aa57e27aa5652db14e415ddf109504", null ],
    [ "read_Euler", "classimu__driver_1_1BNO055.html#a87aadf765dad4748932c97fc8b4a5625", null ],
    [ "read_omega", "classimu__driver_1_1BNO055.html#a822a26f374d633baf1fa6584c4c4887b", null ],
    [ "set_mode", "classimu__driver_1_1BNO055.html#aadd6287add55f2a4a1b4c4101d91224c", null ],
    [ "write_calibrationCoef", "classimu__driver_1_1BNO055.html#a7c1086c91cda8281a7559d7c9b3ee3d6", null ]
];