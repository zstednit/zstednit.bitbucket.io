var classvector_1_1KinematicVector =
[
    [ "__init__", "classvector_1_1KinematicVector.html#a2a61c3f92a65dbb545e69d330583044c", null ],
    [ "getOmegaX", "classvector_1_1KinematicVector.html#aa9abbe4764f2c0b8980aa0482abccf39", null ],
    [ "getOmegaY", "classvector_1_1KinematicVector.html#aed641ddbb909cff767e0fcf41a0ab062", null ],
    [ "getThetaX", "classvector_1_1KinematicVector.html#a383373a26554e11d11959e061deda99f", null ],
    [ "getThetaY", "classvector_1_1KinematicVector.html#aa735018853e5447d7b8ca8006eea82cb", null ],
    [ "getX", "classvector_1_1KinematicVector.html#afcd4942195bbba99c07f8d0c26888065", null ],
    [ "getXDot", "classvector_1_1KinematicVector.html#ad01062e858ad5152750092c82b3c5e1f", null ],
    [ "getY", "classvector_1_1KinematicVector.html#af92409bca3863e59ee16ae2509530e29", null ],
    [ "getYDot", "classvector_1_1KinematicVector.html#a5974a2051ebb4f517d55885b521ef944", null ],
    [ "setErrOmegaX", "classvector_1_1KinematicVector.html#a3cfa365fc85d5fc487cc5d43b10ebe3c", null ],
    [ "setErrOmegaY", "classvector_1_1KinematicVector.html#a2a8fccfb8b04464e7f188c1a8c62fe70", null ],
    [ "setErrThetaX", "classvector_1_1KinematicVector.html#a8335705866478a5b73b702755bdbc414", null ],
    [ "setErrThetaY", "classvector_1_1KinematicVector.html#abe9ecb0baca1bbb1da6421d107b77dd3", null ],
    [ "setOmegaX", "classvector_1_1KinematicVector.html#adee05b0460f3ce1965b982282f40d5c7", null ],
    [ "setOmegaY", "classvector_1_1KinematicVector.html#acf1308b3c6c3f72ce67aa088b0389fbc", null ],
    [ "setThetaX", "classvector_1_1KinematicVector.html#a6efec9139ffc8bb1a7ce59996725f0be", null ],
    [ "setThetaY", "classvector_1_1KinematicVector.html#a9363402a89d6308e8fe14778a4fe60b9", null ],
    [ "setX", "classvector_1_1KinematicVector.html#a7527175ac03c9abc0965f136ccf6038a", null ],
    [ "setXDot", "classvector_1_1KinematicVector.html#ad1b3a2a03a480a1015a5346f335ef10e", null ],
    [ "setY", "classvector_1_1KinematicVector.html#a26b2822d109eac65bcd9935f6db71d90", null ],
    [ "setYDot", "classvector_1_1KinematicVector.html#a7313e81719ad1daf86e21ca8bc9096ed", null ]
];