var files_dup =
[
    [ "BNO055.py", "BNO055_8py.html", [
      [ "BNO055.BNO055", "classBNO055_1_1BNO055.html", "classBNO055_1_1BNO055" ]
    ] ],
    [ "closed_loop_controller.py", "closed__loop__controller_8py.html", [
      [ "closed_loop_controller.closedLoopController", "classclosed__loop__controller_1_1closedLoopController.html", "classclosed__loop__controller_1_1closedLoopController" ]
    ] ],
    [ "DRV8847.py", "DRV8847_8py.html", "DRV8847_8py" ],
    [ "encoder.py", "encoder_8py.html", [
      [ "encoder.Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "gainVector.py", "gainVector_8py.html", [
      [ "gainVector.GainVector", "classgainVector_1_1GainVector.html", "classgainVector_1_1GainVector" ]
    ] ],
    [ "Homework2and3.py", "Homework2and3_8py.html", null ],
    [ "imu_driver.py", "imu__driver_8py.html", "imu__driver_8py" ],
    [ "Lab1Main.py", "Lab1Main_8py.html", "Lab1Main_8py" ],
    [ "main.py", "main_8py.html", null ],
    [ "panelDriver.py", "panelDriver_8py.html", "panelDriver_8py" ],
    [ "shares.py", "shares_8py.html", [
      [ "shares.Share", "classshares_1_1Share.html", "classshares_1_1Share" ],
      [ "shares.Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ]
    ] ],
    [ "task_closed_loop_controller.py", "task__closed__loop__controller_8py.html", "task__closed__loop__controller_8py" ],
    [ "task_encoder.py", "task__encoder_8py.html", "task__encoder_8py" ],
    [ "task_IMU.py", "task__IMU_8py.html", "task__IMU_8py" ],
    [ "task_motor.py", "task__motor_8py.html", "task__motor_8py" ],
    [ "task_motorDriver.py", "task__motorDriver_8py.html", "task__motorDriver_8py" ],
    [ "task_panel.py", "task__panel_8py.html", "task__panel_8py" ],
    [ "task_user.py", "task__user_8py.html", "task__user_8py" ],
    [ "vector.py", "vector_8py.html", [
      [ "vector.KinematicVector", "classvector_1_1KinematicVector.html", "classvector_1_1KinematicVector" ]
    ] ],
    [ "zs_fibonacci.py", "zs__fibonacci_8py.html", "zs__fibonacci_8py" ]
];